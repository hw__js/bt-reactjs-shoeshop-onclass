import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h2 className="card-text">
              {price.toLocaleString("en-US", {
                style: "currency",
                currency: "USD",
              })}
            </h2>
          </div>
          <div>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleClick(this.props.data);
              }}
            >
              Show Detail Info
            </button>
            <button
              onClick={() => {
                this.props.handleBuyShoe(this.props.data);
              }}
              className="btn btn-danger"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}
