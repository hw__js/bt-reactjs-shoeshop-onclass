import React, { Component } from "react";

export default class Cart extends Component {
  renderTBody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ width: "50px" }} src={item.image} alt="" />
          </td>
          <td>
            {(item.price * item.number).toLocaleString("en-US", {
              style: "currency",
              currency: "USD",
            })}
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeleteAdd(item.id, false);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{item.number}</span>
            <button
              onClick={() => {
                this.props.handleDeleteAdd(item.id, true);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeleteShoe(item);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };

  renderTFoot = () => {
    return (
      <tr>
        <td colSpan={4}></td>
        <td>Total:</td>
        <td>
          {this.props.cart
            .reduce((total, item) => {
              return (total += item.number * item.price);
            }, 0)
            .toLocaleString("en-US", {
              style: "currency",
              currency: "USD",
            })}
        </td>
      </tr>
    );
  };

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Img</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>{this.renderTBody()}</tbody>
        <tfoot>{this.renderTFoot()}</tfoot>
      </table>
    );
  }
}
