import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };

  handleChangeDetailShoe = (shoe) => {
    this.setState({ detail: shoe });
  };

  handleAddToCart = (shoe) => {
    // init an update cart from origin cart(cart array)
    let cloneCart = [...this.state.cart];

    // find position of needing product
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });

    // Needing product is not in the cart
    if (index === -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    }
    // Contrast: amount of needing product will add to 1
    else {
      cloneCart[index].number++;
    }
    this.setState({ cart: cloneCart });
  };

  handleDeleteAdd = (idShoe, trueFalse) => {
    // init a new array from origin cart(cart array)
    let cloneCart = [...this.state.cart];

    // find position of needing product
    let index = cloneCart.findIndex((item) => item.id === idShoe);

    if (trueFalse) {
      cloneCart[index].number++;
    } else {
      // Product has been deleted (-1 product) in the cart when user clicks
      cloneCart[index].number--;
      if (cloneCart[index].number === 0) {
        // if product'amount equal to zero, delete product out of cart
        cloneCart = this.state.cart.filter((item) => item.id !== idShoe);
      }
    }

    // update cart state and render cart to interface
    this.setState({ cart: cloneCart });
  };

  handleDeleteShoe = (shoe) => {
    // Delete product out of cart
    let cloneCart = this.state.cart.filter((item) => item.id !== shoe.id);

    // update cart state and render cart to interface
    this.setState({ cart: cloneCart });
  };

  render() {
    return (
      <div className="container py-5">
        <Cart
          handleDeleteShoe={this.handleDeleteShoe}
          handleDeleteAdd={this.handleDeleteAdd}
          cart={this.state.cart}
        />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetailShoe={this.handleChangeDetailShoe}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
